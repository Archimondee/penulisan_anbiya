export default spotlight = [
    {
        "id": 1,
        "image": require('../src/img/spotlight/jawhead_spotlight.jpg'),
        "link" : "https://youtube.com"
    },{
        "id": 2,
        "image": require('../src/img/spotlight/lesley_spotlight.jpg'),
        "link" : "https://youtube.com"
    },{
        "id": 3,
        "image": require('../src/img/spotlight/angela_spotlight.jpg'),
        "link" : "https://youtube.com"
    },{
        "id": 4,
        "image": require('../src/img/spotlight/pharsa_spotlight.jpg'),
        "link" : "https://youtube.com"
    }
]