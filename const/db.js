//0 = Hero Baru
//1 = Assasin
//2 = Tank
//3 = Fighter
//4 = Mage
//5 = Marksman
//6 = Support

export default data = [
    {
        "role" : 1,
        "name" : "Fanny",
        "image" : require('../src/img/fanny.jpg'),
        "thumbnail":require('../src/img/thumbnail/thumbnail_fanny.jpg'),
        "counter":{
            "weak":[
                {
                    id:1,
                    name:"Alucard",
                    image: require('../src/img/alucard.jpg')
                },{
                    id:2,
                    name:"Fanny",
                    image: require('../src/img/fanny.jpg')
                },{
                    id:3,
                    name:"Franco",
                    image: require('../src/img/franco.jpg')
                },
            ],
            "combo":[
                {
                    id:1,
                    name:"Akai",
                    image: require('../src/img/akai.png')
                },
                {
                    id:2,
                    name:"Alpha",
                    image: require('../src/img/alpha.jpg')
                },
                {
                    id:3,
                    name:"Argus",
                    image: require('../src/img/argus.png')
                },
            ],
            "strong":[
                {
                    id:1,
                    name:"Gussion",
                    image: require('../src/img/gussion.png')
                },{
                    id:2,
                    name:"Bane",
                    image: require('../src/img/bane.jpg')
                },{
                    id:3,
                    name:"Balmond",
                    image: require('../src/img/balmond.jpg')
                },
            ]
        },
        "skill":{
            "skill_satu" : {
                "description" : "Damage Fanny saat terbang dapat bertambah 15-30% tergantung kecepatan terbangnya, dan akan meninggalkan Prey mark ke target yang dapat ditumpuk sebanyak 2 kali. Menyerang target yang memiliki Prey mark akan memulihkan energy ke Fanny sebanyak 10 poin per tumpukannya.",
                "name" : "Passive - Air Superiority",
                "type" : "Passive",
                "image": require('../src/img/skill/fanny/skill1.png')
            },
            "skill_dua" : {
                "description" : "Memutar pedang dan menimbulkan 260(+90% Physical Attack) physical damage ke musuh di sekitar.",
                "name" : "Skill 1 - Tornado Strike",
                "type" : "Physical Damage",
                "image": require('../src/img/skill/fanny/skill2.png')
            },
            "skill_tiga" : {
                "description" : "Melempar kabel yang menarik Fanny ke rintangan yang dikenai pertama kali. Setiap penggunaan beruntun dalam 3 detik akan mengurangi pemakaian Energi sebesar 1 poin. Bila memiliki Energi yang cukup, Tornado Strike akan dikeluarkan.",
                "name" : "Skill 2 - Steel Cable",
                "type" : "Utility",
                "image": require('../src/img/skill/fanny/skill3.png')
            },
            "skill_empat" : {
                "description" : "Menyerang musuh dengan 500(+240% Physical Attack) physical damage. Bila mengenai musuh yang memiliki Prey mark, damage akan bertambah sebesar 20% per tumpukannya.",
                "name" : "Skill 3 - Cut Throat",
                "type" : "Physical Damage",
                "image": require('../src/img/skill/fanny/skill4.png')
            },
        },
        "build" : {
            "items":[
                {
                    "name" : "Bloodlust Axe",
                    "image" : require('../src/img/items/bloodlust.png')
                },{
                    "name" : "Warrior Boots",
                    "image" : require('../src/img/items/warrior_boots.png')
                },{
                    "name" : "Rose Gold Meteor",
                    "image" : require('../src/img/items/rose_gold_meteor.png')
                },{
                    "name" : "Athena Shield",
                    "image" : require('../src/img/items/athena_shield.png')
                },{
                    "name" : "Demons Advent",
                    "image" : require('../src/img/items/demons_advent.png')
                },{
                    "name" : "Wings of the Apocalypse",
                    "image" : require('../src/img/items/wings_apocalypse.png')
                }
            ],
            "emblem":[
                {
                    "name" : "Custom Assasin",
                    "image" : require('../src/img/emblem/assasin.png')
                },{
                    "name" : "Custom Jungle",
                    "image" : require('../src/img/emblem/jungle.png')
                }
            ],
            "spell" : [{
                "name" : "Retribution",
                "image" : require('../src/img/spell/retribution.png')
            },{
                "name" : "Purify",
                "image": require('../src/img/spell/purify.png')
            }]
        },
        "info":{
            "role" : "Assasin",
            "specialty":[
                {
                    "name" : "Charge"
                },{
                    "name" : "Reap"
                }
            ],
            "overview" : {
                "durability" : "42",
                "offense" : "40",
                "ability" : "92",
                "difficulty" : "70"
            },
            "atribut" : {
                "hp" : "2526",
                "mana" : "0",
                "physical" : "126",
                "hp_regen" : "33",
                "mana_regen" : "0",
                "basic" : "80"
            }
        },
        "pertimbangan" :{
            kelebihan: [
                "Hero paling lincah dalam game, bila mampu menggunakannya",
                "Memiliki potensi burst damage yang besar",
                "Dapat mendapatkan kill dan kabur dengan cepat"
            ],
            kekurangan : [
                "Hero tersulit dalam game",
                "Sangat rentan terhadap efek crowd control"
            ]
        },
    }, {
        "role": 2,
        "name": "Akai",
        "image": require('../src/img/akai.png'),
        "thumbnail": require('../src/img/thumbnail/thumbnail_akai.jpg'),
        "counter": {
            "weak": [
                {
                    id: 1,
                    name: "Kagura",
                    image: require('../src/img/kagura.jpg')
                }, {
                    id: 2,
                    name: "Nana",
                    image: require('../src/img/nana.png')
                }, {
                    id: 3,
                    name: "Diggie",
                    image: require('../src/img/diggie.png')
                }, {
                    id: 4,
                    name: "Eudora",
                    image: require('../src/img/eudora.jpg')
                },
            ],
            "combo": [
                {
                    id: 1,
                    name: "Grock",
                    image: require('../src/img/grock.png')
                },
                {
                    id: 2,
                    name: "Roger",
                    image: require('../src/img/roger.jpg')
                },
                {
                    id: 3,
                    name: "Irithel",
                    image: require('../src/img/irithel.jpg')
                }, {
                    id: 4,
                    name: "Estes",
                    image: require('../src/img/estes.png')
                },
            ],
            "strong": [
                {
                    id: 1,
                    name: "Alucard",
                    image: require('../src/img/alucard.jpg')
                }, {
                    id: 2,
                    name: "Karina",
                    image: require('../src/img/karina.jpg')
                }, {
                    id: 3,
                    name: "Zilong",
                    image: require('../src/img/zilong.jpg')
                }, {
                    id: 4,
                    name: "Lapu Lapu",
                    image: require('../src/img/lapu-lapu.jpg')
                },

            ]
        },
        "skill": {
            "skill_satu": {
                "description": "Setiap kali Akai mengeluarkan skill, dia akan menerima perisai yang menahan 8% damage yang diterima selama 2 detik. Efek ini hanya bisa terjadi setiap 2.5 detik.",
                "name": "Passive - Taichi",
                "type": "Passive",
                "image": require('../src/img/skill/akai/skill1.png')
            },
            "skill_dua": {
                "description": "Melompat ke area target, menghasilkan 300(+50% Physical Attack) physical damage ke musuh di sekitar saat mendarat, target yang ada di area mendarat juga akan terkena stun selama 2 detik.",
                "name": "Skill 1 - Thousand Pounder",
                "type": "Physical Damage",
                "image": require('../src/img/skill/akai/skill2.png')
            },
            "skill_tiga": {
                "description": "Menembakkan gelombang kejut ke arah musuh yang akan meledak bila mengenai musuh atau mencapai jarak maksimum, menghasilkan 350(+120% Physical Attack) poin physical damage, menandai musuh selama 5 detik; serangan biasa pada target yang ditandai akan menambahkan 5% maximum HP Akai sebagai physical damage.",
                "name": "Skill 2 - Blender",
                "type": "Physical Damage",
                "image": require('../src/img/skill/akai/skill3.png')
            },
            "skill_empat": {
                "description": "Berputar seperti tornado selama 3.5 detik dan menyerang target. Setiap tabrakan akan memukul balik target dan memberikan 180(+100% Physical Attack) Magic Damage, damage akan berkurang jika mengenai musuh yang sama. Kecepatan gerak bertambah sebanyak 30 points selama skill berlangsung.",
                "name": "Skill 3 - Hurricane Dance",
                "type": "Physical Damage",
                "image": require('../src/img/skill/akai/skill4.png')
            },
        },
        "build": {
            "items": [
                {
                    "name": "Ancient Ghost Statue",
                    "image": require('../src/img/items/ghost_statue.png')
                }, {
                    "name": "Rapid Boots",
                    "image": require('../src/img/items/rapid_boots.png')
                }, {
                    "name": "Dominance Ice",
                    "image": require('../src/img/items/dominance_ice.png')
                }, {
                    "name": "Cursed Helmet",
                    "image": require('../src/img/items/cursed_helmet.png')
                }, {
                    "name": "Bloodthirsty King",
                    "image": require('../src/img/items/bloodthirsty_king.png')
                }, {
                    "name": "Immortality",
                    "image": require('../src/img/items/immortality.png')
                }
            ],
            "emblem": [
                {
                    "name": "Custom Tank",
                    "image": require('../src/img/emblem/tank.png')
                }, {
                    "name": "Custom Support",
                    "image": require('../src/img/emblem/support.png')
                }
            ],
            "spell": [{
                "name": "Flicker",
                "image": require('../src/img/spell/flicker.png')
            }, {
                "name": "Purify",
                "image": require('../src/img/spell/purify.png')
            }]
        },
        "info": {
            "role": "Tank",
            "specialty": [
                {
                    "name": "Charge"
                }, {
                    "name": "Crowd Control"
                }
            ],
            "overview": {
                "durability": "90",
                "offense": "65",
                "ability": "55",
                "difficulty": "45"
            },
            "atribut": {
                "hp": "2769",
                "mana": "422",
                "physical": "115",
                "hp_regen": "42",
                "mana_regen": "12",
                "basic": "24"
            }
        },
        "pertimbangan": {
            kelebihan: [
                "Sangat tangguh berkat passive nya",
                "Skill pertamanya bisa digunakan untuk initiator atau untuk kabur",
                "Bisa merusak formasi musuh dengan mudah menggunakan Ulti-nya"
            ],
            kekurangan: [
                "Damage output-nya rendah",
                "Terbuka terhadap Crowd Control selama Ultimate"
            ]
        },
    },{
        "role": 3,
        "name": "Alpha",
        "image": require('../src/img/alpha.jpg'),
        "thumbnail": require('../src/img/thumbnail/thumbnail_alpha.jpg'),
        "counter": {
            "weak": [
                {
                    id: 1,
                    name: "Kagura",
                    image: require('../src/img/kagura.jpg')
                }, {
                    id: 2,
                    name: "Nana",
                    image: require('../src/img/nana.png')
                }, {
                    id: 3,
                    name: "Diggie",
                    image: require('../src/img/diggie.png')
                }, {
                    id: 4,
                    name: "Eudora",
                    image: require('../src/img/eudora.jpg')
                },
            ],
            "combo": [
                {
                    id: 1,
                    name: "Grock",
                    image: require('../src/img/grock.png')
                },
                {
                    id: 2,
                    name: "Roger",
                    image: require('../src/img/roger.jpg')
                },
                {
                    id: 3,
                    name: "Irithel",
                    image: require('../src/img/irithel.jpg')
                }, {
                    id: 4,
                    name: "Estes",
                    image: require('../src/img/estes.png')
                },
            ],
            "strong": [
                {
                    id: 1,
                    name: "Alucard",
                    image: require('../src/img/alucard.jpg')
                }, {
                    id: 2,
                    name: "Karina",
                    image: require('../src/img/karina.jpg')
                }, {
                    id: 3,
                    name: "Zilong",
                    image: require('../src/img/zilong.jpg')
                }, {
                    id: 4,
                    name: "Lapu Lapu",
                    image: require('../src/img/lapu-lapu.jpg')
                },

            ]
        },
        "skill": {
            "skill_satu": {
                "description": "Memanggil Beta setiap menggunakan skill untuk menandai target. Beta akan menyerang target secara beruntun setelah 2 tanda, menghasilkan 150(+180% Physical Attack) poin True Damage.",
                "name": "Passive - Beta, Advance!",
                "type": "Passive",
                "image": require('../src/img/skill/alpha/skill1.png')
            },
            "skill_dua": {
                "description": "Menebas ke depan, memberikan 210(+90% Physical Attack) poin physical damage kepada musuh sepanjang jalan dan mengurangi movement speed sebesar 70%, selama 2 detik. Setelah gelombang tebasan meluncur, Beta akan menembak mengikuti arah gelombang, memberikan tambahan 90(+40% Physical Attack) poin physical damage.",
                "name": "Skill 1 - Rotary Impact",
                "type": "Physical Damage",
                "image": require('../src/img/skill/alpha/skill2.png')
            },
            "skill_tiga": {
                "description": "Setelah mengisi tenaga beberapa saat, hero menyerang ke depan dalam area kerucut dan memberikan 160(+170% Physical Attack) poin physical damage kepada musuh dan meperrlambat gerakan mereka. Setiap serangan yang mengenai musuh akan menambahkan attack speed sebesar 5% dan memulihkan 150(+40% Magic Power) HP. Saat pengisian tenaga berakhir, Beta akan menembak memutari area serangan, memberikan tambahan 40(+30% Physical Attack) poin physical damage.",
                "name": "Skill 2 - Force Swing",
                "type": "Physical Damage",
                "image": require('../src/img/skill/alpha/skill3.png')
            },
            "skill_empat": {
                "description": "Mengayunkan tombak cahaya dengan kuat, memberikan efek stun pada target yang diserang. Alpha akan menarik diri ke arah musuh, memberikan 245(+140% Physical Attack) poin physical damage kepada musuh di sepanjang jalan. Ketika tombak cahaya mengenai sasaran, Beta akan menyerang area target dan menghasilkan 205(+80% Physical Attack) poin physical damage.",
                "name": "Skill 3 - Spear of Alpha",
                "type": "Physical Damage",
                "image": require('../src/img/skill/alpha/skill4.png')
            },
        },
        "build": {
            "items": [
                {
                    "name": "Boots of Tranquility",
                    "image": require('../src/img/items/tranquil_boots.png')
                }, {
                    "name": "Bloodlust Axe",
                    "image": require('../src/img/items/bloodlust.png')
                }, {
                    "name": "Blade of the 7 Seas",
                    "image": require('../src/img/items/blade_seas.png')
                }, {
                    "name": "Dominance Ice",
                    "image": require('../src/img/items/cursed_helmet.png')
                }, {
                    "name": "Endless Battle",
                    "image": require('../src/img/items/endless_battle.png')
                }, {
                    "name": "Wings of the Apocalypse",
                    "image": require('../src/img/items/wings_apocalypse.png')
                }
            ],
            "emblem": [
                {
                    "name": "Custom Fighter",
                    "image": require('../src/img/emblem/fighter.png')
                }, {
                    "name": "Custom Jungle",
                    "image": require('../src/img/emblem/jungle.png')
                }
            ],
            "spell": [{
                "name": "Flicker",
                "image": require('../src/img/spell/flicker.png')
            }, {
                "name": "Inspire",
                "image": require('../src/img/spell/inspire.png')
            }]
        },
        "info": {
            "role": "Fighter",
            "specialty": [
                {
                    "name": "Charge"
                }, {
                    "name": "Damage"
                }
            ],
            "overview": {
                "durability": "72",
                "offense": "78",
                "ability": "64",
                "difficulty": "73"
            },
            "atribut": {
                "hp": "2646",
                "mana": "453",
                "physical": "115",
                "hp_regen": "39",
                "mana_regen": "16",
                "basic": "40"
            }
        },
        "pertimbangan": {
            kelebihan: [
                "Skillnya bisa memberikan serangan AoE besar",
                "Bisa menjadi inisiator yang baik dengan ulti - nya",
                "Build hybrid cocok dengan skill 2",
            ],
            kekurangan: [
                "Mudah dibunuh di tahap awal permainan",
                "Sangat bergantung pada item"
            ]
        },
    }, {
        "role": 4,
        "name": "Aurora",
        "image": require('../src/img/aurora.jpg'),
        "thumbnail": require('../src/img/thumbnail/thumbnail_aurora.jpg'),
        "counter": {
            "weak": [
                {
                    id: 1,
                    name: "Kagura",
                    image: require('../src/img/kagura.jpg')
                }, {
                    id: 2,
                    name: "Nana",
                    image: require('../src/img/nana.png')
                }, {
                    id: 3,
                    name: "Diggie",
                    image: require('../src/img/diggie.png')
                }, {
                    id: 4,
                    name: "Eudora",
                    image: require('../src/img/eudora.jpg')
                },
            ],
            "combo": [
                {
                    id: 1,
                    name: "Grock",
                    image: require('../src/img/grock.png')
                },
                {
                    id: 2,
                    name: "Roger",
                    image: require('../src/img/roger.jpg')
                },
                {
                    id: 3,
                    name: "Irithel",
                    image: require('../src/img/irithel.jpg')
                }, {
                    id: 4,
                    name: "Estes",
                    image: require('../src/img/estes.png')
                },
            ],
            "strong": [
                {
                    id: 1,
                    name: "Alucard",
                    image: require('../src/img/alucard.jpg')
                }, {
                    id: 2,
                    name: "Karina",
                    image: require('../src/img/karina.jpg')
                }, {
                    id: 3,
                    name: "Zilong",
                    image: require('../src/img/zilong.jpg')
                }, {
                    id: 4,
                    name: "Lapu Lapu",
                    image: require('../src/img/lapu-lapu.jpg')
                },

            ]
        },
        "skill": {
            "skill_satu": {
                "description": "Setiap mengeluarkan skill, satu unit frost energy akan ditambahkan ke hero, keika 4 units frost energy terkumpul, skill serangan berikutnya akan membekukan target. Semua skill memberikan extra magic damage kepada target yang beku.",
                "name": "Passive - Pride of Ice",
                "type": "Passive",
                "image": require('../src/img/skill/aurora/skill1.png')
            },
            "skill_dua": {
                "description": "Menembakan misil Frost Energy, yang akan meledak ketika mengenai musuh dan memberikan 300(+130% Magic Power) poin Magic Damage secara radial dan memperlambat gerakan lawan.",
                "name": "Skill 1 - Frost Shock",
                "type": "Magical Damage",
                "image": require('../src/img/skill/aurora/skill2.png')
            },
            "skill_tiga": {
                "description": "Memberikan 430(+180% Magic Power) magic damage pada target dan mengurangi kecepatan gerak target sebesar 80% selama 1.5 detik.",
                "name": "Skill 2 - Bitter Shock",
                "type": "Magical Damage",
                "image": require('../src/img/skill/aurora/skill3.png')
            },
            "skill_empat": {
                "description": "Memanggil es raksasa untuk menyerang lokasi sasaran, semua hero musuh yang terkena serangan akan diperlambat dan menerima 900(+180% Magic Power) Magic Damage. Musuh disekitar lokasi sasaran dimana es tersebut jatuh juga akan diperlambat dan menerima 450(+90% Magic Power) poin Magic Damage.",
                "name": "Skill 3 - Coldness Destroy",
                "type": "Magical Damage",
                "image": require('../src/img/skill/aurora/skill4.png')
            },
        },
        "build": {
            "items": [
                {
                    "name": "Enchanted Talisman",
                    "image": require('../src/img/items/enchanted_talisman.png')
                }, {
                    "name": "Arcane Boots",
                    "image": require('../src/img/items/arcane_boots.png')
                }, {
                    "name": "Holy Crystal",
                    "image": require('../src/img/items/blade_seas.png')
                }, {
                    "name": "Fleeting Time",
                    "image": require('../src/img/items/fleeting_time.png')
                }, {
                    "name": "Queen Wand",
                    "image": require('../src/img/items/queen_wand.png')
                }, {
                    "name": "Bloods Wing",
                    "image": require('../src/img/items/bloods_wing.png')
                }
            ],
            "emblem": [
                {
                    "name": "Common Magic",
                    "image": require('../src/img/emblem/magic.png')
                }, {
                    "name": "Custom Mage",
                    "image": require('../src/img/emblem/mage.png')
                }
            ],
            "spell": [{
                "name": "Flicker",
                "image": require('../src/img/spell/flicker.png')
            }, {
                "name": "Purify",
                "image": require('../src/img/spell/purify.png')
            }]
        },
        "info": {
            "role": "Mage",
            "specialty": [
                {
                    "name": "Crowd Control"
                }, {
                    "name": "Burst Damage"
                }
            ],
            "overview": {
                "durability": "40",
                "offense": "45",
                "ability": "90",
                "difficulty": "60"
            },
            "atribut": {
                "hp": "2501",
                "mana": "500",
                "physical": "110",
                "hp_regen": "34",
                "mana_regen": "32",
                "basic": "50"
            }
        },
        "pertimbangan": {
            kelebihan: [
                "Semua skill memperlambat musuh",
                "Bisa menjadi aset yang berharga dengan skill passive - nya",
                "Mengeluarkan damage yang tinggi"
            ],
            kekurangan: [
                "Boros mana di awal game",
                "Assassin bisa mengatasi dia sebelum Aurora bisa berbuat apapun"
            ]
        },
    }, {
        "role": 6,
        "name": "Angela",
        "image": require('../src/img/angela.jpg'),
        "thumbnail": require('../src/img/thumbnail/thumbnail_angela.jpg'),
        "counter": {
            "weak": [
                {
                    id: 1,
                    name: "Hayabusa",
                    image: require('../src/img/hayabusa.jpg')
                }, {
                    id: 2,
                    name: "Lesley",
                    image: require('../src/img/lesley.jpg')
                }, {
                    id: 3,
                    name: "Zhask",
                    image: require('../src/img/zhask.png')
                }
            ],
            "combo": [
                {
                    id: 1,
                    name: "Akai",
                    image: require('../src/img/akai.png')
                },
                {
                    id: 2,
                    name: "Johnson",
                    image: require('../src/img/johnson.jpg')
                },
                {
                    id: 3,
                    name: "Jawhead",
                    image: require('../src/img/jawhead.jpg')
                },
            ],
            "strong": [
                {
                    id: 1,
                    name: "Harley",
                    image: require('../src/img/harley.jpg')
                }, {
                    id: 2,
                    name: "Lapu-Lapu",
                    image: require('../src/img/lapu-lapu.jpg')
                }, {
                    id: 3,
                    name: "Lancelot",
                    image: require('../src/img/lancelot.jpg')
                }

            ]
        },
        "skill": {
            "skill_satu": {
                "description": "Setiap kali Angela memakai skill, movement speednya bertambah sebesar 15% selama 4 detik, dapat ditambah hingga 30%. Teman yang dirasukinya juga mendapatkan efek ini.",
                "name": "Passive - Smart Heart",
                "type": "Passive",
                "image": require('../src/img/skill/angela/skill1.png')
            },
            "skill_dua": {
                "description": "Meluncurkan energi cinta pada arah yang ditentukan, menyebabkan 170(+30% Magic Power) magic damage dan memberikan Lover's Mark. Setiap tumpukan Lover's Mark akan menambah damage yang diterima target sebesar 20% dan mengurangi movement speednya sebesar 10%. Efek ini bertahan selama 4 detik dan dapat ditumpuk sebanyak 5 kali. Selain itu skill ini dapat memulihkan 100(+60% Magic Power) HP hero satu tim. Skill ini memilki maksimum 5 charge (waktu pengisian ulang dipengaruhi cooldown reduction).",
                "name": "Skill 1 - Love Waves",
                "type": "Magical Damage",
                "image": require('../src/img/skill/angela/skill2.png')
            },
            "skill_tiga": {
                "description": "Meluncurkan tali boneka ke target yang ditentukan, menyebabkan 350(+70% Magic Power) magic damage dan berangsur-angsur memperlambat gerakan target hingga 80%. Bila target masih terhubung dengan tali ini setelah 3 detik, mereka akan tidak dapat bergerak selama 1,5 detik dan menerima 450(+60% Magic Power) magic damage. Setiap tumpukan Lover's Mark akan menambah damage tambahan ini sebanyak 450(+60% Magic Power).",
                "name": "Skill 2 - Puppet-on-a-String",
                "type": "Magical Damage",
                "image": require('../src/img/skill/angela/skill3.png')
            },
            "skill_empat": {
                "description": "Angela mengumpulkan energi untuk memberikan perisai ke hero satu tim yang dapat menahan 1200(+200% Magic Power) damage selama 6 detik. Setelah itu, dia akan merasuki tubuh target selama 12 detik (seluruh skill Angela dapat digunakan dalam waktu ini tanpa menggunakan Mana). Bila Angela memakai skill ini lagi, atau bila hero target tadi mati, efek ini akan dibatalkan.",
                "name": "Skill 3 - Heart Guard",
                "type": "Magical Damage",
                "image": require('../src/img/skill/angela/skill4.png')
            },
        },
        "build": {
            "items": [
                {
                    "name": "Enchanted Talisman",
                    "image": require('../src/img/items/enchanted_talisman.png')
                }, {
                    "name": "Magic Shoes",
                    "image": require('../src/img/items/magic_boots.png')
                }, {
                    "name": "Clock of Destiny",
                    "image": require('../src/img/items/blade_seas.png')
                }, {
                    "name": "Courage Bulwark",
                    "image": require('../src/img/items/courage_bulwark.png')
                }, {
                    "name": "Ice Queen Wand",
                    "image": require('../src/img/items/queen_wand.png')
                }, {
                    "name": "Bloods Wing",
                    "image": require('../src/img/items/bloods_wing.png')
                }
            ],
            "emblem": [
                {
                    "name": "Common Magic",
                    "image": require('../src/img/emblem/magic.png')
                }, {
                    "name": "Custom Support",
                    "image": require('../src/img/emblem/support.png')
                }
            ],
            "spell": [{
                "name": "Flicker",
                "image": require('../src/img/spell/flicker.png')
            }, {
                "name": "Weaken",
                "image": require('../src/img/spell/weaken.png')
            }]
        },
        "info": {
            "role": "Support",
            "specialty": [
                {
                    "name": "Regen"
                }, {
                    "name": "Poke"
                }
            ],
            "overview": {
                "durability": "49",
                "offense": "36",
                "ability": "85",
                "difficulty": "32"
            },
            "atribut": {
                "hp": "2421",
                "mana": "515",
                "physical": "115",
                "hp_regen": "34",
                "mana_regen": "18",
                "basic": "60"
            }
        },
        "pertimbangan": {
            kelebihan: [
                "Dapat mengkite musuh dengan mudah dengan skill 1 dan pasifnya",
                "Memiliki potensi damage yang sangat besar dengan Lover's Marknya",
                "Dapat melindungi hero tim dan bergabung dengan teamfight dengan cepat",
            ],
            kekurangan: [
                "Sangat tipis",
                "Dapat membuat terlalu bergantung dengan jumlah Lover's Mark"
            ]
        }
    }, {
        "role": 5,
        "name": "Miya",
        "image": require('../src/img/miya.png'),
        "thumbnail": require('../src/img/thumbnail/thumbnail_miya.jpg'),
        "counter": {
            "weak": [
                {
                    id: 1,
                    name: "Lancelot",
                    image: require('../src/img/lancelot.jpg')
                }, {
                    id: 2,
                    name: "Hayabusa",
                    image: require('../src/img/hayabusa.jpg')
                }, {
                    id: 3,
                    name: "Karina",
                    image: require('../src/img/zhask.png')
                }, {
                    id: 4,
                    name: "Fanny",
                    image: require('../src/img/fanny.jpg')
                }
            ],
            "combo": [
                {
                    id: 1,
                    name: "Grock",
                    image: require('../src/img/grock.png')
                },
                {
                    id: 2,
                    name: "Rafaela",
                    image: require('../src/img/rafaela.jpg')
                },
                {
                    id: 3,
                    name: "Freya",
                    image: require('../src/img/freya.jpg')
                }, ,
                {
                    id: 4,
                    name: "Estes",
                    image: require('../src/img/estes.png')
                },
            ],
            "strong": [
                {
                    id: 1,
                    name: "Balmond",
                    image: require('../src/img/balmond.jpg')
                }, {
                    id: 2,
                    name: "Karrie",
                    image: require('../src/img/karrie.jpg')
                }, {
                    id: 3,
                    name: "Clint",
                    image: require('../src/img/clint.png')
                }, {
                    id: 4,
                    name: "Minotaur",
                    image: require('../src/img/minotaur.jpg')
                }

            ]
        },
        "skill": {
            "skill_satu": {
                "description": "Setiap serangan biasa mengenai target, Attack Speed Miya akan meningkat sebesar 5% selama 4 detik, efek ini dapat ditumpuk sebanyak 8 kali..",
                "name": "Passive - Turbo",
                "type": "Passive",
                "image": require('../src/img/skill/miya/skill1.png')
            },
            "skill_dua": {
                "description": "Setelah skill digunakan, setiap serangan biasa akan menembakkan 2 panah yang memencar, menyebabkan 5(+105% Physical Attack) poin physical damage ke target utama dan 30% damage serangan biasa ke target lainnya. Efek ini bertahan selama 4 detik.",
                "name": "Skill 1 - Fission Shot",
                "type": "Physical Damage",
                "image": require('../src/img/skill/miya/skill2.png')
            },
            "skill_tiga": {
                "description": "Menembakkan rentetan panah ke atas menuju area yang ditentukan, mengenai musuh sebanyak 5 kali di dalam lingkaran. Setiap hit mengakibatkan 30(+15% Physical Attack) poin physical damage. Hujan panah ini akan memperlambat musuh, dan setelah 4 kali hit musuh akan terkunci di tempat selama 1 detik.",
                "name": "Skill 2 - Rain of Arrows",
                "type": "Physical Damage",
                "image": require('../src/img/skill/miya/skill3.png')
            },
            "skill_empat": {
                "description": "Menghilangkan seluruh efek penggangu gerakan pada Miya dan menjadi tak terlihat selama 1,5 detik, meningkatkan attack speed sebanyak 35% selama 6 detik dan movement speed sebesar 45% selama 1,5 detik.",
                "name": "Skill 3 - Turbo Stealth",
                "type": "Utility/ Buff",
                "image": require('../src/img/skill/miya/skill4.png')
            },
        },
        "build": {
            "items": [
                {
                    "name": "Haas Claws",
                    "image": require('../src/img/items/haas_claws.png')
                }, {
                    "name": "Swift Boots",
                    "image": require('../src/img/items/swift_boots.png')
                }, {
                    "name": "Berserker Fury",
                    "image": require('../src/img/items/berserker_fury.png')
                }, {
                    "name": "Windtalker",
                    "image": require('../src/img/items/windtalker.png')
                }, {
                    "name": "Malefic Roar",
                    "image": require('../src/img/items/malefic_roar.png')
                }, {
                    "name": "Demon Hunter",
                    "image": require('../src/img/items/demon_hunter.png')
                }
            ],
            "emblem": [
                {
                    "name": "Custom Marksman",
                    "image": require('../src/img/emblem/marksman.png')
                }, {
                    "name": "Common Physical",
                    "image": require('../src/img/emblem/physical.png')
                }
            ],
            "spell": [{
                "name": "Inspire",
                "image": require('../src/img/spell/inspire.png')
            }, {
                "name": "Heal",
                "image": require('../src/img/spell/heal.png')
            }]
        },
        "info": {
            "role": "Marksman",
            "specialty": [
                {
                    "name": "Reap"
                }, {
                    "name": "Burst"
                }
            ],
            "overview": {
                "durability": "42",
                "offense": "95",
                "ability": "62",
                "difficulty": "25"
            },
            "atribut": {
                "hp": "2524",
                "mana": "445",
                "physical": "117",
                "hp_regen": "30",
                "mana_regen": "15",
                "basic": "60"
            }
        },
        "pertimbangan": {
            kelebihan: [
                "Memiliki kecepatan serangan tercepat dalam skill Passive - nya.",
                "Skill ultimate dapat digunakan sebagai alat untuk menyerbu dan melarikan diri.",
                "Berkontribusi dalam pertarungan antar tim dengan efek AoE."
            ],
            kekurangan: [
                "Sangat mudah dibunuh di awal permainan, masih lemah dalam durasi permainan yang lama",
                "Terkena stun biasanya berujung pada kematian."
            ]
        }
    }
]