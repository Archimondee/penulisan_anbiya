import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, AsyncStorage } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { Container, Content, Card, CardItem, Body, Button, Right, Left, Footer } from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import data from '../../const/db';

export default class DrawerContainer extends Component {
    constructor(props){
        super(props);
        this.state={
            data : data
        }
    }
    _heroAssasin = () => {
        const dataHero = this.state.data;
        const dataAssasin = [];
        dataHero.map((items, i) => {
            if (items.role === 1) {
                dataAssasin.push(items);
            }
        })
        return dataAssasin;
    }
    _heroTank = () => {
        const dataHero = this.state.data;
        const dataTank = [];
        dataHero.map((items, i) => {
            if (items.role === 2) {
                dataTank.push(items);
            }
        })
        return dataTank;
    }
    _heroFighter = () => {
        const dataHero = this.state.data;
        const dataFighter = [];
        dataHero.map((items, i) => {
            if (items.role === 3) {
                dataFighter.push(items);
            }
        })
        return dataFighter;
    }
    _heroMage = () => {
        const dataHero = this.state.data;
        const dataMage = [];
        dataHero.map((items, i) => {
            if (items.role === 4) {
                dataMage.push(items);
            }
        })
        return dataMage;
    }
    _heroMarksman = () => {
        const dataHero = this.state.data;
        const dataMarksman = [];
        dataHero.map((items, i) => {
            if (items.role === 5) {
                dataMarksman.push(items);
            }
        })
        return dataMarksman;
    }
    _heroSupport = () => {
        const dataHero = this.state.data;
        const dataSupport = [];
        dataHero.map((items, i) => {
            if (items.role === 6) {
                dataSupport.push(items);
            }
        })
        return dataSupport;
    }
    render() {
        const { navigation } = this.props;
        return (
            <Container>
                <Content padder>
                    <View style={{}}>
                        <View style={{ marginTop: 30, marginBottom: 5 }}>
                            <Image
                                source={require('../../assets/mobile_legends.jpg')}
                                style={{ width: '100%', height: 150 }}
                                resizeMode="contain"
                            />
                        </View>
                        
                        <View style={{ marginTop: 25 }}>
                            <Button style={{ width: '100%', backgroundColor: 'gray' }} onPress={() => navigation.navigate('List',{data:this._heroAssasin(), role:'Assasin'})}>
                                <Left style={{ margin: 5, marginLeft: 10}}>
                                    <Text style={{ fontSize: 18 }}>Hero Assasin</Text>
                                </Left>
                            </Button>
                            <Button style={{ width: '100%', backgroundColor: 'gray', marginTop: 5 }} onPress={() => navigation.navigate('List', { data: this._heroTank(), role: 'Tank' })} >
                                <Left style={{ margin: 5, marginLeft: 10 }}>
                                    <Text style={{ fontSize: 18 }}>Hero Tanker</Text>
                                </Left>
                            </Button>
                            <Button style={{ width: '100%', backgroundColor: 'gray', marginTop: 5 }} onPress={() => navigation.navigate('List', { data: this._heroMage(), role: 'Mage' })}>
                                <Left style={{ margin: 5, marginLeft: 10}}>
                                    <Text style={{ fontSize: 18 }}>Hero Mage</Text>
                                </Left>
                            </Button>
                            <Button style={{ width: '100%', backgroundColor: 'gray', marginTop: 5 }} onPress={() => navigation.navigate('List', { data: this._heroFighter(), role: 'Fighter' })}>
                                <Left style={{ margin: 5, marginLeft:10 }}>
                                    <Text style={{ fontSize: 18 }}>Hero Fighter</Text>
                                </Left>
                            </Button>
                            <Button style={{ width: '100%', backgroundColor: 'gray', marginTop: 5 }} onPress={() => navigation.navigate('List', { data: this._heroSupport(), role: 'Support' })}>
                                <Left style={{ margin: 5, marginLeft: 10 }}>
                                    <Text style={{ fontSize: 18 }}>Hero Support</Text>
                                </Left>
                            </Button>
                            <Button style={{ width: '100%', backgroundColor: 'gray', marginTop: 5 }} onPress={() => navigation.navigate('List', { data: this._heroMarksman(), role: 'Marksman' })}>
                                <Left style={{ margin: 5, marginLeft: 10}}>
                                    <Text style={{ fontSize: 18 }}>Hero Marksman</Text>
                                </Left>
                            </Button>
                        </View>
                    </View>
                </Content>
                <Footer style={{ width: '100%', backgroundColor: 'white', flexDirection: 'column', justifyContent: 'center', alignContent: 'center', alignItems: 'center', alignSelf: 'center' }}>
                    <Text>Mobile Legends Bang Bang</Text>
                    <Text>Moonton All Right Reserved</Text>
                    <Text>2018</Text>
                </Footer>
            </Container>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ccc',
        paddingTop: 40,
        paddingHorizontal: 20,
    },
    uglyDrawerItem: {
        fontSize: 20,
        color: 'blue',
        padding: 15,
        margin: 5,
        borderRadius: 10,
        borderColor: 'blue',
        borderWidth: 1,
        textAlign: 'center',
        backgroundColor: 'white',
        overflow: 'hidden',
    },
});

