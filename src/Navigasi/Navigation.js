import { createStackNavigator, createDrawerNavigator} from 'react-navigation';
import HeroScreen from '../components/Hero/HeroScreen';
import HomeScreen from '../components/Home/HomeScreen';
import ListScreen from '../components/HeroList/ListScreen';
import DrawerContainer from './DrawerContainer';
import React from 'react';
import {
    Text,
    Animated,
    Easing
} from 'react-native';
import { Button } from 'native-base';

const noTransitionConfig = () => ({
    transitionSpec: {
        duration: 0,
        timing: Animated.timing,
        easing: Easing.step0,
    },
});
const DrawerStack = createDrawerNavigator({
    Home: {screen: HomeScreen}
},{
    gesturesEnabled: false,
    contentComponent: props => <DrawerContainer {...props} />,
})

export default Main = createStackNavigator({
    Drawer: {screen: DrawerStack},
    Hero: {screen: HeroScreen},
    List: {screen: ListScreen}
},{
    initialRouteName: 'Drawer',
    headerMode: 'none'
})