import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import {
    Container,
    Content,
    Header,
    Card,
    CardItem,
    Left,
    Right,
    Body
} from 'native-base';
import data from '../../../const/db';
import ImageComponent from './ImageComponent';
import { Ionicons } from '@expo/vector-icons';
export default class ListScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            data:this.props.navigation.getParam('data'),
            role:this.props.navigation.getParam('role')
        }
    }
  render() {
      let images = this.state.data.map((items, i)=>{
          return(
              <TouchableOpacity key={i} onPress={() => this.props.navigation.navigate("Hero", { 'counter': items.counter, 'name': items.name, 'thumbnail': items.thumbnail, 'skill': items.skill, 'build': items.build, "info": items.info, pertimbangan: items.pertimbangan })}>
                  <View style={styles.imageWrap} >
                      <ImageComponent imgsource={items.image}></ImageComponent>
                  </View>
                  <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5 }} >
                      <Text style={{ width: 0, flexGrow: 1, flex: 1, textAlign: 'center' }}>
                          {items.name}
                      </Text>
                  </View>
              </TouchableOpacity>
          )
      })
    return (
        <Container>
            <Header style={{ backgroundColor: 'gray' }}>
                <Left>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                        <Ionicons name="ios-arrow-back" size={40} color="white" />
                    </TouchableOpacity>
                    
                </Left>
                <Body>
                    <Text style={{ color: 'white', fontWeight: 'bold' }}>Hero {this.state.role}</Text>
                </Body>
                <Right />
            </Header>
            <Content>
                <View style={styles.container}>
                    {
                        images
                    }
                </View>
            </Content>
        </Container>
    );
  }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        backgroundColor: '#eee'
    },
    imageWrap: {
        margin: 5,
        padding: 5,
        height: (Dimensions.get('window').height / 3) - 12,
        width: (Dimensions.get('window').width / 2) - 12,
        backgroundColor: '#fff'

    }
})