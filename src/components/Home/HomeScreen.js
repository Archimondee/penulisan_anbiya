import React, {Component} from 'react';
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Text,
  Icon,
  Right,
  Left,
  Body,
  Button
} from 'native-base';
import {Image, StyleSheet, View, TouchableOpacity, Dimensions, Linking} from 'react-native';
import Swiper from 'react-native-swiper';
import spotlight from '../../../const/db_spotlight';
import data from '../../../const/db';
import {Ionicons} from '@expo/vector-icons';

export default class HomeScreen extends Component {
  constructor(props){
    super(props);
    this.state={
      spotlight:spotlight,
      data: data
    }
  }
  _heroAssasin = () =>{
    const dataHero = this.state.data;
    const dataAssasin = [];
    dataHero.map((items, i)=>{
      if(items.role === 1){
        dataAssasin.push (items);
      }
    })
    return dataAssasin;
  }
  _heroTank = () => {
    const dataHero = this.state.data;
    const dataTank = [];
    dataHero.map((items, i)=>{
      if(items.role === 2){
        dataTank.push(items);
      }
    })
    return dataTank;
  }
  _heroFighter = () => {
    const dataHero = this.state.data;
    const dataFighter = [];
    dataHero.map((items, i) => {
      if (items.role === 3) {
        dataFighter.push(items);
      }
    })
    return dataFighter;
  }
  _heroMage = () => {
    const dataHero = this.state.data;
    const dataMage = [];
    dataHero.map((items, i) => {
      if (items.role === 4) {
        dataMage.push(items);
      }
    })
    return dataMage;
  }
  _heroMarksman = () => {
    const dataHero = this.state.data;
    const dataMarksman = [];
    dataHero.map((items, i) => {
      if (items.role === 5) {
        dataMarksman.push(items);
      }
    })
    return dataMarksman;
  }
  _heroSupport = () => {
    const dataHero = this.state.data;
    const dataSupport = [];
    dataHero.map((items, i) => {
      if (items.role === 6) {
        dataSupport.push(items);
      }
    })
    return dataSupport;
  }
  render () {
    return (
      <Container>
        <Header style={{backgroundColor:'gray'}}>
          <Left>
            <Button onPress={() => this.props.navigation.openDrawer()} style={{ paddingLeft: 12 }} transparent><Ionicons name='ios-menu' size={30} /></Button>
          </Left>
          <Body>
            <Text style={{color:'black', fontWeight:'bold'}}>Mobile Legends</Text>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <View style={{marginTop:20}}>
            <Swiper style={{height:250}} autoplay={true}>
            {
              spotlight.map((items, i)=>{
                return(
                  <TouchableOpacity  style={{flex:1,}} key={items.id} onPress={()=>Linking.openURL(items.link)}>
                      <Image
                        style={{ flex:1, height:null, width:null, resizeMode:'contain'}}
                        source={items.image}
                      />
                  </TouchableOpacity>
                )
              })
            }
            </Swiper>
          </View>
          <Card style={{marginTop:10}}>
            <CardItem header style={{backgroundColor:'gray'}}>
              <Left><Text>Hero Assasin</Text></Left>
              <Right><TouchableOpacity onPress={() => { this.props.navigation.navigate('List', { data: this._heroAssasin(), role:'Assasin' }) }}><Text>View All</Text></TouchableOpacity></Right>
            </CardItem>
            <CardItem cardBody style={{marginBottom:10}}>
              {
                this._heroAssasin().map((items, i)=>{
                  return(
                    <TouchableOpacity key={i+1} onPress={()=>{this.props.navigation.navigate('Hero',{'counter' : items.counter, 'name':items.name, 'thumbnail':items.thumbnail, 'skill': items.skill, 'build': items.build, "info":items.info, pertimbangan:items.pertimbangan})}}>
                      <View style={styles.imageWrap}>
                        <Image
                          source={items.image}
                          style={{flex:1, width:null, alignSelf:"stretch"}}
                        />
                      </View>
                      <View style={{flexDirection:'row', marginTop:1, marginBottom:5}} >
                        <Text style={{width:0, flexGrow:1, flex:1, textAlign:'center'}}>
                                {items.name}
                        </Text>
                    </View>
                    </TouchableOpacity>
                  )  
                })
              }
            </CardItem>
            <CardItem header style={{ backgroundColor: 'gray' }}>
              <Left><Text>Hero Tank</Text></Left>
              <Right><TouchableOpacity onPress={() => { this.props.navigation.navigate('List', { data: this._heroTank(), role: 'Tank' }) }}><Text>View All</Text></TouchableOpacity></Right>
            </CardItem>
            <CardItem cardBody style={{marginBottom:10}}>
              {
                this._heroTank().map((items, i)=>{
                  return(
                    <TouchableOpacity key={i + 1} onPress={() => { this.props.navigation.navigate('Hero', { 'counter': items.counter, 'name': items.name, 'thumbnail': items.thumbnail, 'skill': items.skill, 'build': items.build, "info": items.info, pertimbangan: items.pertimbangan }) }}>
                      <View style={styles.imageWrap}>
                        <Image
                          source={items.image}
                          style={{ flex: 1, width: null, alignSelf: "stretch" }}
                        />
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 1, marginBottom: 5 }} >
                        <Text style={{ width: 0, flexGrow: 1, flex: 1, textAlign: 'center' }}>
                          {items.name}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  )  
                })
              }
            </CardItem>

            <CardItem header style={{ backgroundColor: 'gray' }}>
              <Left><Text>Hero Fighter</Text></Left>
              <Right><TouchableOpacity onPress={() => { this.props.navigation.navigate('List', { data: this._heroFighter(), role: 'Fighter' }) }}><Text>View All</Text></TouchableOpacity></Right>
            </CardItem>
            <CardItem cardBody style={{ marginBottom: 10 }}>
              {
                this._heroFighter().map((items, i) => {
                  return (
                    <TouchableOpacity key={i + 1} onPress={() => { this.props.navigation.navigate('Hero', { 'counter': items.counter, 'name': items.name, 'thumbnail': items.thumbnail, 'skill': items.skill, 'build': items.build, "info": items.info, pertimbangan: items.pertimbangan }) }}>
                      <View style={styles.imageWrap}>
                        <Image
                          source={items.image}
                          style={{ flex: 1, width: null, alignSelf: "stretch" }}
                        />
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 1, marginBottom: 5 }} >
                        <Text style={{ width: 0, flexGrow: 1, flex: 1, textAlign: 'center' }}>
                          {items.name}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  )
                })
              }
            </CardItem>

            <CardItem header style={{ backgroundColor: 'gray' }}>
              <Left><Text>Hero Mage</Text></Left>
              <Right><TouchableOpacity onPress={() => { this.props.navigation.navigate('List', { data: this._heroMage(), role: 'Mage' }) }}><Text>View All</Text></TouchableOpacity></Right>
            </CardItem>
            <CardItem cardBody style={{ marginBottom: 10 }}>
              {
                this._heroMage().map((items, i) => {
                  return (
                    <TouchableOpacity key={i + 1} onPress={() => { this.props.navigation.navigate('Hero', { 'counter': items.counter, 'name': items.name, 'thumbnail': items.thumbnail, 'skill': items.skill, 'build': items.build, "info": items.info, pertimbangan: items.pertimbangan }) }}>
                      <View style={styles.imageWrap}>
                        <Image
                          source={items.image}
                          style={{ flex: 1, width: null, alignSelf: "stretch" }}
                        />
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 1, marginBottom: 5 }} >
                        <Text style={{ width: 0, flexGrow: 1, flex: 1, textAlign: 'center' }}>
                          {items.name}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  )
                })
              }
            </CardItem>

            <CardItem header style={{ backgroundColor: 'gray' }}>
              <Left><Text>Hero Support</Text></Left>
              <Right><TouchableOpacity onPress={() => { this.props.navigation.navigate('List', { data: this._heroSupport(), role: 'Support' }) }}><Text>View All</Text></TouchableOpacity></Right>
            </CardItem>
            <CardItem cardBody style={{ marginBottom: 10 }}>
              {
                this._heroSupport().map((items, i) => {
                  return (
                    <TouchableOpacity key={i + 1} onPress={() => { this.props.navigation.navigate('Hero', { 'counter': items.counter, 'name': items.name, 'thumbnail': items.thumbnail, 'skill': items.skill, 'build': items.build, "info": items.info, pertimbangan: items.pertimbangan }) }}>
                      <View style={styles.imageWrap}>
                        <Image
                          source={items.image}
                          style={{ flex: 1, width: null, alignSelf: "stretch" }}
                        />
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 1, marginBottom: 5 }} >
                        <Text style={{ width: 0, flexGrow: 1, flex: 1, textAlign: 'center' }}>
                          {items.name}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  )
                })
              }
            </CardItem>

            <CardItem header style={{ backgroundColor: 'gray' }}>
              <Left><Text>Hero Marksman</Text></Left>
              <Right><TouchableOpacity onPress={() => { this.props.navigation.navigate('List', { data: this._heroMarksman(), role: 'Marksman' }) }}><Text>View All</Text></TouchableOpacity></Right>
            </CardItem>
            <CardItem cardBody style={{ marginBottom: 10 }}>
              {
                this._heroMarksman().map((items, i) => {
                  return (
                    <TouchableOpacity key={i + 1} onPress={() => { this.props.navigation.navigate('Hero', { 'counter': items.counter, 'name': items.name, 'thumbnail': items.thumbnail, 'skill': items.skill, 'build': items.build, "info": items.info, pertimbangan: items.pertimbangan }) }}>
                      <View style={styles.imageWrap}>
                        <Image
                          source={items.image}
                          style={{ flex: 1, width: null, alignSelf: "stretch" }}
                        />
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 1, marginBottom: 5 }} >
                        <Text style={{ width: 0, flexGrow: 1, flex: 1, textAlign: 'center' }}>
                          {items.name}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  )
                })
              }
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    //backgroundColor: '#eee',
  },
  imageWrap: {
    margin: 5,
    padding: 5,
    height: Dimensions.get ('window').height / 6 - 12,
    width: Dimensions.get ('window').width / 4.2 - 12,
    backgroundColor: '#fff',
    borderWidth: 0.8,
  },
});
