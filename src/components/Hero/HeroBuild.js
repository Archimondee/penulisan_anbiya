import React from 'react';
import {Text, View} from 'react-native';
import {createMaterialTopTabNavigator} from 'react-navigation';

import EmblemScreen from './HeroBuild/EmblemScreen';
import ItemScreen from './HeroBuild/ItemScreen';
import SpellScreen from './HeroBuild/SpellScreen';

export default (HeroBuild = createMaterialTopTabNavigator ({
  Item: {screen: props => <ItemScreen {...props} />},
  Emblem: {screen: props => <EmblemScreen {...props} />},
  Spell: {screen: props => <SpellScreen {...props} />},
}));
