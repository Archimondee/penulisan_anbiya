import React from 'react';
import {Text, View} from 'react-native';
import {createMaterialTopTabNavigator} from 'react-navigation';

import SkillSatu from './HeroSkill/SkillSatu';
import SkillDua from './HeroSkill/SkillDua';
import SkillTiga from './HeroSkill/SkillTiga';
import SkillEmpat from './HeroSkill/SkillEmpat';

export default HeroSkill = createMaterialTopTabNavigator ({
  Skill1: {screen: props=><SkillSatu {...props}/>},
  Skill2: {screen:props=><SkillDua {...props}/>},
  Skill3: {screen:props=><SkillTiga {...props}/>},
  Skill4: {screen:props=><SkillEmpat {...props}/>},
});