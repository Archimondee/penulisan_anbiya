import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
} from 'react-native';
import {Container, Content} from 'native-base';

export default class VictimScreen extends React.Component {
  constructor (props) {
    super (props);
    this.state = {
      strong: this.props.screenProps.strong,
    };
  }
  render () {
    //console.log (this.props);
    return (
      <Container>
        <Content>
          <View style={styles.container}>
            {this.state.strong.map ((items, i) => {
              return (
                <TouchableOpacity key={i + 1}>
                  <View style={styles.imageWrap}>
                    <Image
                      source={items.image}
                      style={{flex: 1, width: null, alignSelf: 'stretch'}}
                    />
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      marginTop: 1,
                      marginBottom: 5,
                    }}
                  >
                    <Text
                      style={{
                        width: 0,
                        flexGrow: 1,
                        flex: 1,
                        textAlign: 'center',
                      }}
                    >
                      {items.name}
                    </Text>
                  </View>
                </TouchableOpacity>
              );
            })}

          </View>
        </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create ({
  container: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    //backgroundColor: '#eee',
  },
  imageWrap: {
    margin: 5,
    padding: 5,
    height: Dimensions.get ('window').height / 6 - 12,
    width: Dimensions.get ('window').width / 4.2 - 12,
    backgroundColor: '#fff',
    borderWidth: 0.8,
  },
});
