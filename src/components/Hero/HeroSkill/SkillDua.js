import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import {Card, CardItem} from 'native-base';

export default class SkillSatu extends Component {
  constructor (props) {
    super (props);
    this.state = {
      satu: this.props.screenProps.skill_dua,
    };
  }
  componentDidMount () {
    //console.log(this.state.satu)
  }
  render () {
    return (
      <Card>
        <CardItem
          header
          style={{backgroundColor: 'gray', justifyContent: 'center'}}
        >
          <Text>{this.state.satu.name}</Text>
        </CardItem>
        <CardItem style={{flexDirection: 'row', borderWidth: 1, height: 120}}>
          <View style={{borderRightWidth: 1, width: '28%'}}>
            <Image
              source={this.state.satu.image}
              style={{height: 120, resizeMode: 'contain'}}
            />
          </View>
          <View style={{width: '78%'}}>
            <Text style={{textAlign: 'center'}}>{this.state.satu.type}</Text>
          </View>
        </CardItem>
        <CardItem
          style={{
            borderBottomWidth: 1,
            borderLeftWidth: 1,
            borderRightWidth: 1,
          }}
        >
          <Text>{this.state.satu.description}</Text>
        </CardItem>
      </Card>
    );
  }
}
