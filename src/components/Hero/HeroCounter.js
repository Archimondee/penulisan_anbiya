import React from 'react';
import {Text, View} from 'react-native';
import {createMaterialTopTabNavigator} from 'react-navigation';

import EnemyScreen from './HeroCounter/EnemyScreen';
import ComboScreen from './HeroCounter/ComboScreen';
import VictimsScreen from './HeroCounter/VictimsScren';


export default HeroCounter = createMaterialTopTabNavigator ({
  Weak: {screen: props=><EnemyScreen {...props}/>},
  Combo: {screen:props=><ComboScreen {...props}/>},
  Strong: {screen:props=><VictimsScreen {...props}/>},
});
