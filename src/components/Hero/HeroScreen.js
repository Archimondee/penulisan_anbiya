import React, {Component} from 'react';
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Text,
  Icon,
  Right,
  Left, Body
} from 'native-base';
import {Image, StyleSheet, View, TouchableOpacity, Dimensions, Linking} from 'react-native';
import { Constants } from 'expo';
import {createMaterialTopNavigator} from 'react-navigation';
import HeroCounter from './HeroCounter';
import HeroSkill from './HeroSkill';
import HeroBuild from './HeroBuild';
import {Ionicons} from '@expo/vector-icons';
//import EnemyScreen from './HeroCounter';

export default class HeroScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            counters:this.props.navigation.getParam('counter'),
            name:this.props.navigation.getParam('name'),
            thumbnail:this.props.navigation.getParam('thumbnail'),
            skill:this.props.navigation.getParam('skill'),
            build: this.props.navigation.getParam('build'),
            info: this.props.navigation.getParam('info'),
            pertimbangan: this.props.navigation.getParam('pertimbangan')
        }
    }
    componentDidMount(){
        console.log(this.state.counters.combo);
    }
  render() {
        const contoh = {
            contohProps: "Hello"
        }
        const data = this.state.pertimbangan;
    return (
      <Container style={styles.header}>
        <Header style={{backgroundColor:'gray'}}>
          <Left>
            <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
              <Ionicons name="ios-arrow-back" size={40} color="white"/>
            </TouchableOpacity>
          </Left>
          <Body>
            <Text style={{color:'white', fontWeight:'bold', fontSize:15}}>Hero {this.state.name}</Text>
          </Body>
        </Header>
        <Content>
            <Card>
                <CardItem header style={{backgroundColor:'gray'}}>
                    <Text style={{color:'white'}}>{this.state.name}</Text>
                </CardItem>
                <CardItem cardBody style={{height:200, width:'100%'}}>
                    <Image source={this.state.thumbnail} 
                        style={{flex:1, height:"100%", width:"100%", resizeMode:'contain'}} 
                        />
                </CardItem>
                <CardItem header style={{backgroundColor:'gray'}}>
                    <Text style={{color:'white'}}>
                        Friend and Foes
                    </Text>
                </CardItem>
                <CardItem cardBody style={{height:270, width:"100%"}}>
                    <HeroCounter screenProps={this.state.counters}/>
                </CardItem>
                <CardItem header style={{backgroundColor:'gray'}}>
                    <Text style={{color:'white'}}>
                        Hero Skill
                    </Text>
                </CardItem>
                <CardItem cardBody style={{height:450, width:"100%"}}>
                    <HeroSkill screenProps={this.state.skill}/>
                </CardItem>
                <CardItem header style={{backgroundColor:'gray', marginTop:10}}>
                    <Text style={{color:'white'}}>Info {this.state.name}</Text>
                </CardItem>
                <CardItem cardBody style={{}}>
                    <View 
                    style={{justifyContent:'center', alignContent:'center', 
                        alignItems:'center', alignSelf:'center',
                        flexDirection:'column', margin:10, width:"95%"}}>
                        <CardItem header style={{borderWidth:1, borderColor:'black',backgroundColor:'gray', width:"100%", justifyContent:'center', alignItems:'center'}}>
                            <Text style={{textAlign:'center', color:'white'}}>Role</Text>
                        </CardItem>
                        <CardItem style={{borderWidth:1, borderColor:'black', width:"100%", justifyContent:'center', alignItems:'center'}}>
                            <Text>{this.state.info.role}</Text>
                        </CardItem>
                    </View>
                </CardItem>
                <CardItem cardBody style={{}}>
                    <View 
                    style={{justifyContent:'center', alignContent:'center', 
                        alignItems:'center', alignSelf:'center',
                        flexDirection:'column', margin:10, width:"95%"}}>
                        <CardItem header style={{borderWidth:1, borderColor:'black',backgroundColor:'gray', width:"100%", justifyContent:'center', alignItems:'center'}}>
                            <Text style={{textAlign:'center', color:'white'}}>Specialty</Text>
                        </CardItem>
                        <View style={{flexDirection:'row', borderWidth:1, borderColor:'black', width:"100%",
                            justifyContent:'center'}}> 
                        {
                            this.state.info.specialty.map((items, i)=>{
                                return(
                                    <View key={i+1} style={{alignItems:'center', borderRightWidth:1, width:"50%"}}>
                                        <Text style={{textAlign:'center'}}>{items.name}</Text>
                                    </View>
                                )
                            })
                        }
                        </View>
                    </View>
                </CardItem>
                <CardItem cardBody style={{}}>
                    <View 
                    style={{justifyContent:'center', alignContent:'center', 
                        alignItems:'center', alignSelf:'center',
                        flexDirection:'column', margin:10, width:"95%"}}>
                        <CardItem header style={{borderWidth:1, borderColor:'black',backgroundColor:'gray', width:"100%", justifyContent:'center', alignItems:'center'}}>
                            <Text style={{textAlign:'center', color:'white'}}>Overview</Text>
                        </CardItem>
                        <View style={{flexDirection:'row', borderWidth:1, borderColor:'black', width:"100%",
                            justifyContent:'center',
                        }}> 
                            <View style={{alignItems:'center', borderRightWidth:1, width:"25%"}}>
                                <View style={{flexDirection:'column'}}>
                                    <View style={{flexDirection:'row', height:65, width:"100%", borderBottomWidth:1, padding:5}} >
                                            <Text style={{width:0, flexGrow:1, flex:1, textAlign:'center'}}>
                                                Durability
                                            </Text>
                                        </View>
                                    <View>
                                        <Text style={{textAlign:'center'}}>{this.state.info.overview.durability}</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{alignItems:'center', borderRightWidth:1, width:"25%"}}>
                                <View style={{flexDirection:'column'}}>
                                    <View style={{flexDirection:'row', height:65, width:"100%", borderBottomWidth:1, padding:5}} >
                                            <Text style={{width:0, flexGrow:1, flex:1, textAlign:'center'}}>
                                                Offense
                                            </Text>
                                        </View>
                                    <View>
                                        <Text style={{textAlign:'center'}}>{this.state.info.overview.offense}</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{alignItems:'center', borderRightWidth:1, width:"25%"}}>
                                <View style={{flexDirection:'column'}}>
                                    <View style={{flexDirection:'row', height:65, width:"100%", borderBottomWidth:1, padding:5}} >
                                            <Text style={{width:0, flexGrow:1, flex:1, textAlign:'center'}}>
                                                Ability Efect
                                            </Text>
                                        </View>
                                    <View>
                                        <Text style={{textAlign:'center'}}>{this.state.info.overview.ability}</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{alignItems:'center', borderRightWidth:1, width:"25%"}}>
                                <View style={{flexDirection:'column'}}>
                                    <View style={{flexDirection:'row', height:65, width:"100%", borderBottomWidth:1, padding:5}} >
                                            <Text style={{width:0, flexGrow:1, flex:1, textAlign:'center'}}>
                                                Difficulty
                                            </Text>
                                        </View>
                                    <View>
                                        <Text style={{textAlign:'center'}}>{this.state.info.overview.difficulty}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </CardItem>
                <CardItem cardBody style={{}}>
                    <View 
                    style={{justifyContent:'center', alignContent:'center', 
                        alignItems:'center', alignSelf:'center',
                        flexDirection:'column', margin:10, width:"95%"}}>
                        <CardItem header style={{borderWidth:1, borderColor:'black',backgroundColor:'gray', width:"100%", justifyContent:'center', alignItems:'center'}}>
                            <Text style={{textAlign:'center', color:'white'}}>Atribut Alucard</Text>
                        </CardItem>
                        <View style={{flexDirection:'row', borderWidth:1, borderColor:'black', width:"100%",
                            justifyContent:'center',
                        }}> 
                            <View style={{alignItems:'center', borderRightWidth:1, width:"25%"}}>
                                <View style={{flexDirection:'column'}}>
                                    <View style={{flexDirection:'row', height:65, width:"100%", borderBottomWidth:1, padding:5}} >
                                            <Text style={{width:0, flexGrow:1, flex:1, textAlign:'center'}}>
                                                HP
                                            </Text>
                                        </View>
                                    <View>
                                        <Text style={{textAlign:'center'}}>Mana</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{alignItems:'center', borderRightWidth:1, width:"25%"}}>
                                <View style={{flexDirection:'column'}}>
                                    <View style={{flexDirection:'row', height:65, width:"100%", borderBottomWidth:1, padding:5}} >
                                            <Text style={{width:0, flexGrow:1, flex:1, textAlign:'center'}}>
                                                {this.state.info.atribut.hp}
                                            </Text>
                                        </View>
                                    <View>
                                        <Text style={{textAlign:'center'}}>{this.state.info.atribut.mana}</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{alignItems:'center', borderRightWidth:1, width:"25%"}}>
                                <View style={{flexDirection:'column'}}>
                                    <View style={{flexDirection:'row', height:65, width:"100%", borderBottomWidth:1, padding:5}} >
                                            <Text style={{width:0, flexGrow:1, flex:1, textAlign:'center'}}>
                                                HP Regen
                                            </Text>
                                        </View>
                                    <View style={{flexDirection:'row', height:65, width:"100%", borderBottomWidth:1, padding:5}} >
                                            <Text style={{width:0, flexGrow:1, flex:1, textAlign:'center'}}>
                                                Mana Regen
                                            </Text>
                                        </View>
                                </View>
                            </View>
                            <View style={{alignItems:'center', borderRightWidth:1, width:"25%"}}>
                                <View style={{flexDirection:'column'}}>
                                    <View style={{flexDirection:'row', height:65, width:"100%", borderBottomWidth:1, padding:5}} >
                                            <Text style={{width:0, flexGrow:1, flex:1, textAlign:'center'}}>
                                                {this.state.info.atribut.hp_regen}
                                            </Text>
                                        </View>
                                    <View>
                                        <Text style={{textAlign:'center'}}>{this.state.info.atribut.mana_regen}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={{flexDirection:'row', borderWidth:1, borderColor:'black', width:"100%",
                            justifyContent:'center',
                        }}> 
                            <View style={{alignItems:'center', borderRightWidth:1, width:"25%"}}>
                                <View style={{flexDirection:'column'}}>
                                    <View style={{flexDirection:'row', height:65, width:"100%", borderBottomWidth:1, padding:5}} >
                                            <Text style={{width:0, flexGrow:1, flex:1, textAlign:'center'}}>
                                                Physical
                                            </Text>
                                        </View>
                                    
                                </View>
                            </View>
                            <View style={{alignItems:'center', borderRightWidth:1, width:"25%"}}>
                                <View style={{flexDirection:'column'}}>
                                    <View style={{flexDirection:'row', height:65, width:"100%", borderBottomWidth:1, padding:5}} >
                                            <Text style={{width:0, flexGrow:1, flex:1, textAlign:'center'}}>
                                                {this.state.info.atribut.physical}
                                            </Text>
                                        </View>
                                </View>
                            </View>
                            <View style={{alignItems:'center', borderRightWidth:1, width:"25%"}}>
                                <View style={{flexDirection:'column'}}>
                                    <View style={{flexDirection:'row', height:65, width:"100%", borderBottomWidth:1, padding:5}} >
                                            <Text style={{width:0, flexGrow:1, flex:1, textAlign:'center'}}>
                                                Basic
                                            </Text>
                                        </View>
                                </View>
                            </View>
                            <View style={{alignItems:'center', borderRightWidth:1, width:"25%"}}>
                                <View style={{flexDirection:'column'}}>
                                    <View style={{flexDirection:'row', height:65, width:"100%", borderBottomWidth:1, padding:5}} >
                                            <Text style={{width:0, flexGrow:1, flex:1, textAlign:'center'}}>
                                                {this.state.info.atribut.basic}
                                            </Text>
                                        </View>
                                </View>
                            </View>
                        </View>
                        
                    </View>
                </CardItem>
                <CardItem header style={{backgroundColor:'gray'}}>
                    <Text style={{color:'white'}}>
                        Build For {this.state.name}
                    </Text>
                </CardItem>
                <CardItem cardBody style={{height:400}}>
                    <HeroBuild screenProps={this.state.build}/>
                </CardItem>
                <CardItem header style={{backgroundColor:'gray'}}>
                    <Text style={{color:'white'}}>Kelebihan dan Kekurangan</Text>
                </CardItem>
                <CardItem cardBody style={{marginBottom:20}}>
                    <View style={{flexDirection:'column', marginLeft:10, width:"98%"}}>
                        <View style={{backgroundColor:'gray', marginTop:15, marginRight:10}}>
                            <Text style={{paddingLeft:10}}>Kelebihan</Text>
                        </View>
                        {
                            this.state.pertimbangan.kelebihan.map((items, i)=>{
                                return(<View key={i+1} style={{flexDirection:'row'}}>
                                    <Text>{i+1}. </Text>
                                    <Text style={{paddingLeft: 5, flex:1}}>{items}</Text>
                                </View>)
                            })
                        }
                        
                        <View style={{backgroundColor:'gray', marginTop:15, marginRight:10}}>
                            <Text style={{paddingLeft:10}}>Kekurangan</Text>
                        </View>
                        {
                            this.state.pertimbangan.kekurangan.map((items, i)=>{
                                return(<View key={i+1} style={{flexDirection:'row'}}>
                                    <Text>{i+1}. </Text>
                                    <Text style={{paddingLeft: 5, flex:1}}>{items}</Text>
                                </View>)
                            })
                        }
                    </View>
                </CardItem>
            </Card>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create ({
  scene: {
    flex: 1,
  },
  header: {
    paddingTop: Constants.statusBarHeight,
    //backgroundColor:'gray'
  },
});
