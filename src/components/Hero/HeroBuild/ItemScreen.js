import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
} from 'react-native';
import {Container, Content} from 'native-base';

export default class ItemScreen extends React.Component {
  constructor (props) {
    super (props);
    this.state={
      items:this.props.screenProps.items
    }
  }
  componentDidMount(){
    //console.log(this.state.items)
  }
  render () {
    //console.log (this.props);
    return (
      <Container>
        <Content>
          <View style={{flexDirection:'row', marginLeft:5, marginBottom:20, flexWrap:'wrap'}}>
          {
            this.state.items.map((lol, i)=>{
              return(
                  <View key={i+1} style={{flexDirection:'column', width:'33%', borderWidth:1, justifyContent:'center', alignContent:'center', alignItems:'center', alignSelf:'center'}}>
                    <Image source={lol.image} style={{resizeMode:'contain'}}/>
                    <Text style={{textAlign:'center'}}>{lol.name}</Text>
                  </View>  
              )
            })
          }
               
          </View>
        </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create ({
  container: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    //backgroundColor: '#eee',
  },
  imageWrap: {
    margin: 5,
    padding: 5,
    height: Dimensions.get ('window').height / 6 - 12,
    width: Dimensions.get ('window').width / 4.2 - 12,
    backgroundColor: '#fff',
    borderWidth: 0.8,
  },
});
